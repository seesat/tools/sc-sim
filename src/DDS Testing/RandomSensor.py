import fastdds
import TestSim
from random import randint
from time import sleep

# Initialize DDS entities for publication
domain_participant_factory = fastdds.DomainParticipantFactory.get_instance()
default_participant_qos = fastdds.DomainParticipantQos()
domain_participant = domain_participant_factory.create_participant(0, default_participant_qos)

default_publisher_qos = fastdds.PublisherQos()
publisher = domain_participant.create_publisher(default_publisher_qos)

topic_data_type = TestSim.SensorDataPubSubType()
topic_data_type.setName("SensorData")
# Register the type with the domain participant
type_support = fastdds.TypeSupport(topic_data_type)
domain_participant.register_type(type_support)

topic = domain_participant.create_topic("SensorDataTopic", topic_data_type.getName(), fastdds.TOPIC_QOS_DEFAULT)
data_writer = publisher.create_datawriter(topic, fastdds.DATAWRITER_QOS_DEFAULT)


# Function to simulate sensor data generation and sending
def publish_sensor_data():
    while True:
        # Generate random sensor data
        sensor_data = TestSim.SensorData()
        sensor_data.value(randint(1, 100))
        sensor_data.name("RandomSensor")

        # Publish the data
        data_writer.write(sensor_data)

        # Wait for 2 seconds before sending the next data
        sleep(2)


if __name__ == "__main__":
    publish_sensor_data()
