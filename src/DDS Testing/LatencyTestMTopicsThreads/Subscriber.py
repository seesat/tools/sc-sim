import fastdds
from datetime import datetime
from ExampleType import ExampleType
import threading


class DataReaderThread(threading.Thread):
    def __init__(self, participant, topic, log_file_name):
        threading.Thread.__init__(self)
        self.data_reader = None
        self.subscriber = None
        self.topic = topic
        self.participant = participant
        self.log_file_name = log_file_name

    def create_subscriber(self):
        self.subscriber = self.participant.create_subscriber_with_profile("subscriber_profile")
        self.data_reader = self.subscriber.create_datareader_with_profile(self.topic, "datareader_profile")
        #print(f"Subscriber created for topic {self.topic.get_name()}")

    def run(self):
        self.create_subscriber()
        sample_info = fastdds.SampleInfo()
        data = ExampleType()
        biggest_latency = 0
        total_latency = 0
        count = 0

        while count < 100:
            if self.data_reader.take_next_sample(data, sample_info) == fastdds.ReturnCode_t.RETCODE_OK:
                receive_time = datetime.now().timestamp()
                latency = receive_time - float(data.time_string())
                if latency > biggest_latency:
                    biggest_latency = latency
                total_latency += latency
                count += 1
                # print(f"Message {count} received at {receive_time} on topic {self.topic.get_name()} with latency {latency}")

        with open(self.log_file_name, 'w') as log_file:
            average_latency = total_latency / count
            log_file.write(f"Average Latency: {average_latency * 1000:.3f} ms\n")
            log_file.write(f"The biggest latency was {biggest_latency * 1000:.3f} ms\n")
        #print(f"Reader for topic {self.topic.get_name()} finished. Log written to {self.log_file_name}")
