import fastdds
from ExampleType import ExampleType, ExampleTypePubSubType

# Load the XML profiles
xml_file = "profiles.xml"
if fastdds.DomainParticipantFactory.get_instance().load_XML_profiles_file(xml_file) == fastdds.ReturnCode_t.RETCODE_OK:
    # Create a DomainParticipant using the loaded profile
    participant = fastdds.DomainParticipantFactory.get_instance().create_participant_with_profile(0,
                                                                                                  "participant_profile")

    example_type = ExampleTypePubSubType()
    example_type.setName("ExampleData")
    type_support = fastdds.TypeSupport(example_type)
    participant.register_type(type_support)

    topic = participant.create_topic("example_topic", example_type.getName(), fastdds.TOPIC_QOS_DEFAULT)

    # Create a Publisher using the loaded profile
    publisher = participant.create_publisher_with_profile("publisher_profile")
    data_writer = publisher.create_datawriter_with_profile(topic, "datawriter_profile")

    # Create a Subscriber using the loaded profile
    subscriber = participant.create_subscriber_with_profile("subscriber_profile")
    data_reader = subscriber.create_datareader_with_profile(topic, "datareader_profile")
