import time
import random
import threading
import fastdds
from datetime import datetime
from ExampleType import ExampleType


class DataWriterThread(threading.Thread):
    def __init__(self, participant, topic):
        threading.Thread.__init__(self)
        self.participant = participant
        self.topic = topic
        self.publisher = None
        self.data_writer = None

    def create_publisher(self):
        self.publisher = self.participant.create_publisher_with_profile("publisher_profile")
        self.data_writer = self.publisher.create_datawriter_with_profile(self.topic, "datawriter_profile")
        print(f"Publisher created for topic {self.topic.get_name()}")

    def run(self):
        self.create_publisher()
        start_time = time.time()
        while time.time() - start_time < 120:  # Run for 20 seconds
            data = ExampleType()
            data.value(0)  # Set a dummy value, if needed change accordingly
            data.time_string(f"{datetime.now().timestamp()}")
            self.data_writer.write(data)
            # print(f"Message sent at {data.time_string()} on topic {self.topic.get_name()}")
            time.sleep(random.uniform(0.01, 0.1))
        print(f"Writer for topic {self.topic.get_name()} finished")
