import os
import subprocess
import time
import sys

def run_sim_master(num_writers, num_subscribers, topics):
    command = ['python3', 'SimMaster.py', str(num_writers), str(num_subscribers)] + topics
    print(f"Running: {' '.join(command)}")
    subprocess.run(command, check=True)

def main(num_topics):
    num_writers = 1
    subscribers_list = [1, 2, 4, 8, 16, 32]
    topics_list = [f"topic{i+1}" for i in range(num_topics)]  # Creates topic1, topic2, ..., topicN

    for num_subscribers in subscribers_list:
        run_sim_master(num_writers, num_subscribers, topics_list)
        time.sleep(5)  # Adding a small delay to ensure the system stabilizes

    # Call the plot script
    plot_pattern = f"{num_topics}Topics"
    print(f"Plotting results for pattern: {plot_pattern}")
    subprocess.run(['python3', 'plot.py', plot_pattern], check=True)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 run_all_tests.py <number_of_topics>")
        sys.exit(1)

    num_topics = int(sys.argv[1])
    main(num_topics)
