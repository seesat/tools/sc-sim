# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _ExampleTypeWrapper
else:
    import _ExampleTypeWrapper

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)


class SwigPyIterator(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _ExampleTypeWrapper.delete_SwigPyIterator

    def value(self):
        return _ExampleTypeWrapper.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _ExampleTypeWrapper.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _ExampleTypeWrapper.SwigPyIterator_decr(self, n)

    def distance(self, x):
        return _ExampleTypeWrapper.SwigPyIterator_distance(self, x)

    def equal(self, x):
        return _ExampleTypeWrapper.SwigPyIterator_equal(self, x)

    def copy(self):
        return _ExampleTypeWrapper.SwigPyIterator_copy(self)

    def next(self):
        return _ExampleTypeWrapper.SwigPyIterator_next(self)

    def __next__(self):
        return _ExampleTypeWrapper.SwigPyIterator___next__(self)

    def previous(self):
        return _ExampleTypeWrapper.SwigPyIterator_previous(self)

    def advance(self, n):
        return _ExampleTypeWrapper.SwigPyIterator_advance(self, n)

    def __eq__(self, x):
        return _ExampleTypeWrapper.SwigPyIterator___eq__(self, x)

    def __ne__(self, x):
        return _ExampleTypeWrapper.SwigPyIterator___ne__(self, x)

    def __iadd__(self, n):
        return _ExampleTypeWrapper.SwigPyIterator___iadd__(self, n)

    def __isub__(self, n):
        return _ExampleTypeWrapper.SwigPyIterator___isub__(self, n)

    def __add__(self, n):
        return _ExampleTypeWrapper.SwigPyIterator___add__(self, n)

    def __sub__(self, *args):
        return _ExampleTypeWrapper.SwigPyIterator___sub__(self, *args)
    def __iter__(self):
        return self

# Register SwigPyIterator in _ExampleTypeWrapper:
_ExampleTypeWrapper.SwigPyIterator_swigregister(SwigPyIterator)

FASTCDR_VERSION_MAJOR = _ExampleTypeWrapper.FASTCDR_VERSION_MAJOR
FASTCDR_VERSION_MINOR = _ExampleTypeWrapper.FASTCDR_VERSION_MINOR
FASTCDR_VERSION_MICRO = _ExampleTypeWrapper.FASTCDR_VERSION_MICRO
FASTCDR_VERSION_STR = _ExampleTypeWrapper.FASTCDR_VERSION_STR
HAVE_CXX11 = _ExampleTypeWrapper.HAVE_CXX11
FASTCDR_IS_BIG_ENDIAN_TARGET = _ExampleTypeWrapper.FASTCDR_IS_BIG_ENDIAN_TARGET
FASTCDR_HAVE_FLOAT128 = _ExampleTypeWrapper.FASTCDR_HAVE_FLOAT128
FASTCDR_SIZEOF_LONG_DOUBLE = _ExampleTypeWrapper.FASTCDR_SIZEOF_LONG_DOUBLE
import fastdds
class _ExampleTypeSeq(fastdds.LoanableCollection):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _ExampleTypeWrapper.delete__ExampleTypeSeq

# Register _ExampleTypeSeq in _ExampleTypeWrapper:
_ExampleTypeWrapper._ExampleTypeSeq_swigregister(_ExampleTypeSeq)

class ExampleTypeSeq(_ExampleTypeSeq):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    __swig_destroy__ = _ExampleTypeWrapper.delete_ExampleTypeSeq

    def __init__(self, *args):
        _ExampleTypeWrapper.ExampleTypeSeq_swiginit(self, _ExampleTypeWrapper.new_ExampleTypeSeq(*args))

    def __len__(self):
        return _ExampleTypeWrapper.ExampleTypeSeq___len__(self)

    def __getitem__(self, i):
        return _ExampleTypeWrapper.ExampleTypeSeq___getitem__(self, i)

# Register ExampleTypeSeq in _ExampleTypeWrapper:
_ExampleTypeWrapper.ExampleTypeSeq_swigregister(ExampleTypeSeq)

class ExampleType(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    __swig_destroy__ = _ExampleTypeWrapper.delete_ExampleType

    def __init__(self, *args):
        _ExampleTypeWrapper.ExampleType_swiginit(self, _ExampleTypeWrapper.new_ExampleType(*args))

    def __eq__(self, x):
        return _ExampleTypeWrapper.ExampleType___eq__(self, x)

    def __ne__(self, x):
        return _ExampleTypeWrapper.ExampleType___ne__(self, x)

    def value(self, *args):
        return _ExampleTypeWrapper.ExampleType_value(self, *args)

    def time_string(self, *args):
        return _ExampleTypeWrapper.ExampleType_time_string(self, *args)

# Register ExampleType in _ExampleTypeWrapper:
_ExampleTypeWrapper.ExampleType_swigregister(ExampleType)

GEN_API_VER = _ExampleTypeWrapper.GEN_API_VER
class ExampleTypePubSubType(fastdds.TopicDataType):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _ExampleTypeWrapper.ExampleTypePubSubType_swiginit(self, _ExampleTypeWrapper.new_ExampleTypePubSubType())
    __swig_destroy__ = _ExampleTypeWrapper.delete_ExampleTypePubSubType

    def serialize(self, *args):
        return _ExampleTypeWrapper.ExampleTypePubSubType_serialize(self, *args)

    def deserialize(self, payload, data):
        return _ExampleTypeWrapper.ExampleTypePubSubType_deserialize(self, payload, data)

    def getSerializedSizeProvider(self, *args):
        return _ExampleTypeWrapper.ExampleTypePubSubType_getSerializedSizeProvider(self, *args)

    def getKey(self, data, ihandle, force_md5=False):
        return _ExampleTypeWrapper.ExampleTypePubSubType_getKey(self, data, ihandle, force_md5)

    def createData(self):
        return _ExampleTypeWrapper.ExampleTypePubSubType_createData(self)

    def deleteData(self, data):
        return _ExampleTypeWrapper.ExampleTypePubSubType_deleteData(self, data)

    def is_bounded(self):
        return _ExampleTypeWrapper.ExampleTypePubSubType_is_bounded(self)

    def is_plain(self, *args):
        return _ExampleTypeWrapper.ExampleTypePubSubType_is_plain(self, *args)

    def construct_sample(self, memory):
        return _ExampleTypeWrapper.ExampleTypePubSubType_construct_sample(self, memory)
    m_md5 = property(_ExampleTypeWrapper.ExampleTypePubSubType_m_md5_get, _ExampleTypeWrapper.ExampleTypePubSubType_m_md5_set)
    m_keyBuffer = property(_ExampleTypeWrapper.ExampleTypePubSubType_m_keyBuffer_get, _ExampleTypeWrapper.ExampleTypePubSubType_m_keyBuffer_set)

# Register ExampleTypePubSubType in _ExampleTypeWrapper:
_ExampleTypeWrapper.ExampleTypePubSubType_swigregister(ExampleTypePubSubType)



