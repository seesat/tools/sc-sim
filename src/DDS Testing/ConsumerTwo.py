import fastdds
import TestSim
import signal


class SensorDataSubtractListener(fastdds.DataReaderListener):
    def __init__(self):
        super().__init__()

    def on_data_available(self, reader):
        info = fastdds.SampleInfo()
        data = TestSim.SensorData()
        if reader.take_next_sample(data, info) == fastdds.ReturnCode_t.RETCODE_OK:
            print(f"Received: {data.name()} with value {data.value()}")

            # Perform a simple computation: subtract 10
            result = data.value() - 10
            print(f"Computed value after subtracting 10: {result}")


class SensorDataReaderSubtract:
    def __init__(self):
        factory = fastdds.DomainParticipantFactory.get_instance()
        participant_qos = fastdds.DomainParticipantQos()
        factory.get_default_participant_qos(participant_qos)
        self.participant = factory.create_participant(0, participant_qos)

        topic_data_type = TestSim.SensorDataPubSubType()
        topic_data_type.setName("SensorData")
        type_support = fastdds.TypeSupport(topic_data_type)
        self.participant.register_type(type_support)

        topic_qos = fastdds.TopicQos()
        self.participant.get_default_topic_qos(topic_qos)
        self.topic = self.participant.create_topic("SensorDataTopic", topic_data_type.getName(), topic_qos)

        subscriber_qos = fastdds.SubscriberQos()
        self.participant.get_default_subscriber_qos(subscriber_qos)
        self.subscriber = self.participant.create_subscriber(subscriber_qos)

        self.listener = SensorDataSubtractListener()
        reader_qos = fastdds.DataReaderQos()
        self.subscriber.get_default_datareader_qos(reader_qos)
        self.reader = self.subscriber.create_datareader(self.topic, reader_qos, self.listener)

    def delete(self):
        self.participant.delete_contained_entities()
        fastdds.DomainParticipantFactory.get_instance().delete_participant(self.participant)


def signal_handler(sig, frame):
    print('Interrupted, shutting down...')
    global reader_subtract
    reader_subtract.delete()
    exit(0)


if __name__ == '__main__':
    print('Creating subtracting subscriber...')
    reader_subtract = SensorDataReaderSubtract()
    signal.signal(signal.SIGINT, signal_handler)
    print('Press Ctrl+C to stop')
    signal.pause()
