#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Usage: $0 <number_of_subscribers> <topic_name> [<topic_name> ...]"
  exit 1
fi

num_subscribers=$1
shift

for topic in "$@"; do
  echo "Testing topic: $topic"

  # Start the specified number of Subscriber instances with different log file names
  for i in $(seq 1 $num_subscribers); do
    log_file="subscriber_log_${topic}_$i.txt"
    echo "Starting Subscriber instance $i for topic $topic with log file $log_file"
    python3 Subscriber.py "$topic" "$log_file" &
  done

  # Give the subscribers a moment to initialize
  sleep 2

  # Start the Publisher
  echo "Starting Publisher for topic $topic"
  python3 Publisher.py "$topic" &
done

# Wait for all background processes to finish
wait
