import os
import matplotlib.pyplot as plt

def custom_scale(y):
    if y <= 100:
        return y * 0.5  # Linear scaling for y <= 100
    else:
        return 50 + (y - 100) * 0.05  # Scale slower for y > 100

def plot_latency(subscribers, average_latencies, biggest_latencies, num_topics):
    # Sort the data points according to the number of subscribers
    sorted_indices = sorted(range(len(subscribers)), key=lambda k: subscribers[k])
    subscribers = [subscribers[i] for i in sorted_indices]
    average_latencies = [average_latencies[i] for i in sorted_indices]
    biggest_latencies = [biggest_latencies[i] for i in sorted_indices]

    # Apply custom scaling
    scaled_average_latencies = [custom_scale(y) for y in average_latencies]
    scaled_biggest_latencies = [custom_scale(y) for y in biggest_latencies]

    # Plotting
    fig, ax = plt.subplots()

    # Plot average latencies with custom scaling
    ax.plot(subscribers, scaled_average_latencies, marker='o', label='Average Latency', color='blue')

    # Plot biggest latencies with custom scaling
    ax.plot(subscribers, scaled_biggest_latencies, marker='o', label='Biggest Latency', color='red')

    # Adding title and labels
    ax.set_title(f'Testrun with {num_topics} Topics')
    ax.set_xlabel('Number of Subscribers')
    ax.set_ylabel('Scaled Latency (ms)')

    # Adding legend
    ax.legend()

    # Adding grid
    ax.grid(True, which='both', linestyle='--', linewidth=0.5)

    # Custom y-axis ticks and labels
    original_yticks = [0, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 1250, 1500, 1750]
    scaled_yticks = [custom_scale(y) for y in original_yticks]
    ax.set_yticks(scaled_yticks)
    ax.set_yticklabels(original_yticks)

    # Save the plot
    plot_filename = f"latency_plot_{num_topics}_topics.png"
    plt.savefig(plot_filename)
    print(f"Plot saved as {plot_filename}")

def gather_data_from_directories(pattern):
    average_latencies = []
    biggest_latencies = []
    subscribers = []

    for directory in os.listdir(''):
        if os.path.isdir(directory) and pattern in directory:
            num_subscribers = int(directory.split('_')[1].replace('Subs', ''))
            subscribers.append(num_subscribers)
            avg_latencies_for_dir = []
            big_latencies_for_dir = []

            for filename in os.listdir(directory):
                if filename.startswith("subscriber_log_") and filename.endswith(".txt"):
                    with open(os.path.join(directory, filename), 'r') as log_file:
                        lines = log_file.readlines()
                        average_latency = float(lines[0].split(': ')[1].split(' ')[0])
                        biggest_latency = float(lines[1].split(' ')[4])
                        avg_latencies_for_dir.append(average_latency)
                        big_latencies_for_dir.append(biggest_latency)

            if avg_latencies_for_dir:
                average_latencies.append(sum(avg_latencies_for_dir) / len(avg_latencies_for_dir))
            if big_latencies_for_dir:
                biggest_latencies.append(max(big_latencies_for_dir))

    return subscribers, average_latencies, biggest_latencies

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage: python3 plot_results.py <pattern>")
        sys.exit(1)

    pattern = sys.argv[1]
    subscribers, average_latencies, biggest_latencies = gather_data_from_directories(pattern)
    num_topics = int(pattern.split('Topics')[0])
    plot_latency(subscribers, average_latencies, biggest_latencies, num_topics)
