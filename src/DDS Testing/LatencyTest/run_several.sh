#!/bin/bash

# Check if the number of subscribers is provided as an argument
if [ -z "$1" ]; then
  echo "Usage: $0 <number_of_subscribers>"
  exit 1
fi

# Number of subscribers to start
num_subscribers=$1

# Start the specified number of Subscriber instances with different log file names
for i in $(seq 1 $num_subscribers)
do
  log_file="subscriber_log_$i.txt"
  echo "Starting Subscriber instance $i with log file $log_file"
  python3 Subscriber.py "$log_file" &
done

# Give the subscribers a moment to initialize
sleep 2

# Start the Publisher
echo "Starting Publisher"
python3 Publisher.py

# Wait for all background processes to finish
wait
