import sys
import fastdds
from Publisher import DataWriterThread
from Subscriber import DataReaderThread
from ExampleType import ExampleTypePubSubType
import time
import os


def create_run_directory(num_topics, num_subscribers):
    directory_name = f"{num_topics}Topics_{num_subscribers}Subs"
    if not os.path.exists(directory_name):
        os.makedirs(directory_name)
    return directory_name


def main(num_writers, num_readers, topics):
    writer_threads = []
    reader_threads = []
    participant = None
    topic_list = []

    run_directory = create_run_directory(len(topics), num_readers)

    if fastdds.DomainParticipantFactory.get_instance().load_XML_profiles_file(
            "profiles.xml") == fastdds.ReturnCode_t.RETCODE_OK:
        print("Loaded profiles successfully")
        participant = fastdds.DomainParticipantFactory.get_instance().create_participant_with_profile(0,
                                                                                                      "participant_profile")

        example_type = ExampleTypePubSubType()
        example_type.setName("ExampleData")
        type_support = fastdds.TypeSupport(example_type)
        participant.register_type(type_support)
    else:
        raise Exception("Failed to load profiles")

    for topic_name in topics:
        topic = participant.create_topic(topic_name, "ExampleData", fastdds.TOPIC_QOS_DEFAULT)
        topic_list.append(topic)
        print(f"Topic created: {topic_name}")

    for topic in topic_list:
        for i in range(num_writers):
            writer = DataWriterThread(participant, topic)
            writer_threads.append(writer)
            writer.start()
            print(f"Started writer thread {i + 1} for topic {topic.get_name()}")

    time.sleep(2)  # Allow some time for writers to initialize

    for topic in topic_list:
        for i in range(num_readers):
            log_file_name = os.path.join(run_directory, f"subscriber_log_{topic.get_name()}_{i + 1}.txt")
            reader = DataReaderThread(participant, topic, log_file_name)
            reader_threads.append(reader)
            reader.start()
            #print(f"Started reader thread {i + 1} for topic {topic.get_name()}")

    # Wait for all threads to finish
    for writer in writer_threads:
        writer.join()

    for reader in reader_threads:
        reader.join()

    print("All threads have finished. Cleaning up.")
    participant.delete_contained_entities()
    fastdds.DomainParticipantFactory.get_instance().delete_participant(participant)


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python3 SimMaster.py <number_of_writers> <number_of_readers> <topic_name> [<topic_name> ...]")
        sys.exit(1)

    num_writers = int(sys.argv[1])
    num_readers = int(sys.argv[2])
    topics = sys.argv[3:]

    main(num_writers, num_readers, topics)
