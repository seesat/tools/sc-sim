import sys
import time
import random
import fastdds
from datetime import datetime
from ExampleType import ExampleType, ExampleTypePubSubType

def create_publisher(topic_name):
    if fastdds.DomainParticipantFactory.get_instance().load_XML_profiles_file("profiles.xml") == fastdds.ReturnCode_t.RETCODE_OK:
        print("Loaded profiles successfully")
        participant = fastdds.DomainParticipantFactory.get_instance().create_participant_with_profile(0, "participant_profile")

        example_type = ExampleTypePubSubType()
        example_type.setName("ExampleData")
        type_support = fastdds.TypeSupport(example_type)
        participant.register_type(type_support)
        topic = participant.create_topic(topic_name, example_type.getName(), fastdds.TOPIC_QOS_DEFAULT)

        publisher = participant.create_publisher_with_profile("publisher_profile")
        data_writer = publisher.create_datawriter_with_profile(topic, "datawriter_profile")
        return participant, data_writer, example_type
    else:
        raise Exception("Failed to load profiles")

def main(topic_name):
    participant, data_writer, example_type = create_publisher(topic_name)
    for i in range(100):
        data = ExampleType()
        data.value(i)
        data.time_string(f"{datetime.now().timestamp()}")
        data_writer.write(data)
        print(f"{i}st message sent at {data.time_string()}")
        time.sleep(random.uniform(0.01, 0.1))
    participant.delete_contained_entities()
    fastdds.DomainParticipantFactory.get_instance().delete_participant(participant)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 Publisher.py <topic_name>")
        sys.exit(1)
    topic_name = sys.argv[1]
    main(topic_name)
