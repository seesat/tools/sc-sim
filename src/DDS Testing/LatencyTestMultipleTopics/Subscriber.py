import sys
import fastdds
from datetime import datetime
from ExampleType import ExampleType, ExampleTypePubSubType

def create_subscriber(topic_name):
    if fastdds.DomainParticipantFactory.get_instance().load_XML_profiles_file("profiles.xml") == fastdds.ReturnCode_t.RETCODE_OK:
        print("Loaded profiles successfully")
        participant = fastdds.DomainParticipantFactory.get_instance().create_participant_with_profile(0, "participant_profile")

        example_type = ExampleTypePubSubType()
        example_type.setName("ExampleData")
        type_support = fastdds.TypeSupport(example_type)
        participant.register_type(type_support)
        topic = participant.create_topic(topic_name, example_type.getName(), fastdds.TOPIC_QOS_DEFAULT)

        subscriber = participant.create_subscriber_with_profile("subscriber_profile")
        data_reader = subscriber.create_datareader_with_profile(topic, "datareader_profile")
        print("Created Entities")
        return participant, data_reader, example_type
    else:
        raise Exception("Failed to load profiles")

def main(topic_name, log_file_name):
    participant, data_reader, example_type = create_subscriber(topic_name)
    sample_info = fastdds.SampleInfo()
    data = ExampleType()
    biggest_latency = 0
    total_latency = 0
    count = 0

    while count < 99:
        if data_reader.take_next_sample(data, sample_info) == fastdds.ReturnCode_t.RETCODE_OK:
            receive_time = datetime.now().timestamp()
            latency = receive_time - float(data.time_string())
            if latency > biggest_latency:
                biggest_latency = latency
            total_latency += latency
            count = data.value()

    with open(log_file_name, 'w') as log_file:
        average_latency = total_latency / count
        log_file.write(f"Average Latency: {average_latency * 1000:.3f} ms\n")
        log_file.write(f"The biggest latency was {biggest_latency * 1000:.3f} ms\n")

    participant.delete_contained_entities()
    fastdds.DomainParticipantFactory.get_instance().delete_participant(participant)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 Subscriber.py <topic_name> <log_file_name>")
        sys.exit(1)
    topic_name = sys.argv[1]
    log_file_name = sys.argv[2]
    main(topic_name, log_file_name)
